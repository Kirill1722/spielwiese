package com.signedflag.spielwiese.team;

import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface TeamRepository extends CrudRepository<Team, Long> {

    Set<Team> findAll();

    Team findByName(String name);

}
