package com.signedflag.spielwiese.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@RestController
public class TeamController {

    @Autowired
    private TeamRepository teamRepository;

    @PostConstruct
    public void init() {
        Set<Player> players = new HashSet<>();
        players.add(new Player("Max", "blocker"));
        players.add(new Player("Nikk", "goalkeeper"));
        players.add(new Player("Lila", "middle"));
        Team team = new Team("Goodies", "London", players);
        teamRepository.save(team);
    }

    @RequestMapping("/teams/{name}")
    public Team team(@PathVariable String name) {
        return teamRepository.findByName(name);
    }

    @RequestMapping("/teams")
    public Set<Team> teams() {
        return teamRepository.findAll();
    }

}
