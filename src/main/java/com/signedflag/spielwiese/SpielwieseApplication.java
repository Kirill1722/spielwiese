package com.signedflag.spielwiese;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class SpielwieseApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(SpielwieseApplication.class, args);
    }

}
