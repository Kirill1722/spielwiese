package com.signedflag.spielwiese.hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@RestController
public class HelloController {

    @Value("${spielwiese.owner}")
    private String owner;

    @Autowired
    DiscoveryClient client;

    @RequestMapping({"/", "/hi", "/hello"})
    public String hiThere() {
        return "hello world from " + owner;
    }

    @RequestMapping("/echo")
    public String echo() {
        List<ServiceInstance> list = client.getInstances("SPIELWIESE");
        if (list != null && list.size() > 0 ) {
            URI uri = list.get(0).getUri();
            if (uri !=null ) {
                return (new RestTemplate()).getForObject(uri,String.class);
            }
        }
        return null;
    }

}
